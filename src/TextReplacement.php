<?php

namespace Drupal\hanging_conjunctions_filter;

/**
 * Provides text modification methods.
 */
class TextReplacement {

  /**
   * Terms specific for any language typography.
   *
   * @var array
   *   An associated array of key langcode and array of terms.
   */
  protected $terms = [];

  /**
   * Adds non-breaking space after conjunction and specific words.
   *
   * @param string $text
   *   The source text.
   * @param string $langcode
   *   The langcode of the source text.
   *
   * @return string
   *   The output text with non breaking space after special words.
   */
  public function processHangingConjunctions($text, $langcode) {
    if (empty($text)) {
      return $text;
    }

    $this->setTerms();
    if (!$this->getTerms($langcode)) {
      return $text;
    }

    // Tags to skip and not recurse into.
    $ignore_tags = 'a|script|style|code|pre';
    $text = $this->processTags($text, $langcode, $ignore_tags);

    return $text;
  }

  /**
   * Skip ignore tags and add a non-breaking space to the text where necessary.
   *
   * @param string $text
   *   The source text.
   * @param string $langcode
   *   The langcode.
   * @param string $ignore_tags
   *   Tags to skip and not recurse into. Separated by a pipe character (|).
   *
   * @return string
   *   The output text with non-breaking space after special words.
   */
  protected function processTags($text, $langcode, $ignore_tags = '') {
    // Split at all tags. Ensures that no tags or attributes are processed.
    $chunks = preg_split('/(<.+?>)/is', $text, -1, PREG_SPLIT_DELIM_CAPTURE);

    // PHP ensures that the array consists of alternating delimiters and
    // literals, and begins and ends with a literal (inserting NULL as
    // required). Therefore, the first chunk is always text:
    $chunk_type = 'text';

    // If a tag of $ignore_tags is found, it is stored in $open_tag and only
    // removed when the closing tag is found. Until the closing tag is found,
    // no replacements are made.
    $open_tag = '';

    for ($i = 0; $i < count($chunks); $i++) {
      if ($chunk_type == 'text') {
        // Only process this text if there are no unclosed $ignore_tags.
        if ($open_tag == '') {
          // If there is a match, inject a non-breaking space into this chunk.
          $chunks[$i] = $this->insertNonBreakingSpace($chunks[$i], $langcode);
        }
        // Text chunk is done, so next chunk must be a tag.
        $chunk_type = 'tag';
      }
      else {
        // Only process this tag if there are no unclosed $ignore_tags.
        if ($open_tag == '') {
          // Check whether this tag is contained in $ignore_tags.
          if (preg_match("`<($ignore_tags)(?:\s|>)`i", $chunks[$i], $matches)) {
            $open_tag = $matches[1];
          }
        }
        // Otherwise, check whether this is the closing tag for $open_tag.
        else {
          if (preg_match("`<\/$open_tag>`i", $chunks[$i], $matches)) {
            $open_tag = '';
          }
        }
        // Tag chunk is done, so next chunk must be text.
        $chunk_type = 'text';
      }
    }

    $text = implode($chunks);
    return $text;
  }

  /**
   * Adds a non-breaking space to the text where necessary.
   *
   * @param string $text
   *   The source text.
   * @param string $langcode
   *   The langcode of the source text.
   *
   * @return string
   *   The output text with non-breaking space after special words.
   */
  protected function insertNonBreakingSpace($text, $langcode) {
    foreach ($this->terms[$langcode] as $term) {
      $text = preg_replace('/\s(' . $term . ')(\s)/i', ' $1&nbsp;', $text);
      $text = preg_replace('/\s(' . $term . '),(\s)/i', ' $1,&nbsp;', $text);
      $text = preg_replace('/\s(' . $term . ')(\s)-\s/i', ' $1&nbsp;-&nbsp;', $text);
    }

    return $text;
  }

  /**
   * Get terms specific for any language typography.
   *
   * @param string $langcode
   *   The langcode.
   *
   * @return array|null
   *   An array of terms.
   */
  protected function getTerms($langcode) {
    if (isset($this->terms[$langcode])) {
      return $this->terms[$langcode];
    }
    return NULL;
  }

  /**
   * Set terms specific for any language typography.
   */
  protected function setTerms() {
    $terms = [
      'pl' => [
        '—',
        '[a-z]',
        'aby',
        'al.',
        'albo',
        'ale',
        'ależ',
        'ani',
        'aż',
        'b.',
        'bez',
        'bł.',
        'bo',
        'br.',
        'by',
        'bym',
        'byś',
        'co',
        'cyt.',
        'cz.',
        'czy',
        'czyt.',
        'dla',
        'dn.',
        'do',
        'doc.',
        'dr',
        'ds.',
        'dyr.',
        'dz.',
        'fot.',
        'gdy',
        'gdyby',
        'gdybym',
        'gdybyś',
        'gdyż',
        'godz.',
        'im.',
        'inż.',
        'iż',
        'jak',
        'jw.',
        'kol.',
        'komu',
        'ks.',
        'która',
        'którego',
        'której',
        'któremu',
        'który',
        'których',
        'którym',
        'którzy',
        'lecz',
        'lic.',
        'lub',
        'm.in.',
        'max',
        'mgr',
        'min',
        'moich',
        'moje',
        'mojego',
        'mojej',
        'mojemu',
        'mój',
        'mych',
        'na',
        'nad',
        'nie',
        'niech',
        'np.',
        'nr',
        'nr.',
        'nt.',
        'nw.',
        'od',
        'oraz',
        'os.',
        'p.',
        'pl.',
        'pn.',
        'po',
        'pod',
        'pot.',
        'prof.',
        'przed',
        'przez',
        'pt.',
        'pw.',
        'pw.',
        'śp.',
        'św.',
        'tak',
        'tamtej',
        'tamto',
        'te',
        'tej',
        'tel.',
        'tj.',
        'to',
        'twoich',
        'twoje',
        'twojego',
        'twojej',
        'twój',
        'twych',
        'tylko',
        'ul.',
        'we',
        'wg',
        'więc',
        'woj.',
        'za',
        'ze',
        'że',
        'żeby',
        'żebyś',
      ],
    ];

    // Allows to add additional terms.
    \Drupal::moduleHandler()->alter('hanging_conjunction_filter_terms', $terms);

    $this->terms = $terms;
  }

}
