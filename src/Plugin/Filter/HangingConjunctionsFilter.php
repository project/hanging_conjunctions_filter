<?php

namespace Drupal\hanging_conjunctions_filter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\hanging_conjunctions_filter\TextReplacement;

/**
 * A text filter to add a non-breaking space to the text where necessary.
 *
 * @Filter(
 *   id = "hanging_conjunctions_filter",
 *   title = @Translation("Hanging Conjunctions Filter"),
 *   description = @Translation("Moves conjunctions and specific words from the end of the line to the next line."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 50
 * )
 */
class HangingConjunctionsFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (empty($text)) {
      return $result;
    }

    $replacement = new TextReplacement();
    $result->setProcessedText($replacement->processHangingConjunctions($text, $langcode));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Adds non-breaking space after conjunction and specific words.');
  }

}
