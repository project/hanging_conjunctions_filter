<?php

/**
 * @file
 * Hooks provided by the Hanging Conjunctions Filter module.
 */

/**
 * Add terms for any languages.
 *
 * @param array $terms
 *   An associated array of key langcode and array of terms.
 */
function hook_hanging_conjunction_filter_terms_alter(array &$terms) {
  // Add an additional terms for the Polish language.
  array_push($terms['pl'], 'xyz');

  // Add terms for the Czech language.
  $terms['cs'] = [
    '[a-z]',
  ];
}
