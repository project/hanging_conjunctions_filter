INTRODUCTION
------------

Dangling conjunctions, also known as orphans, are a typographical error in the
Polish language. They are single-letter words left at the end of a line. This
rule applies to, for example, conjunctions and prepositions.

This module provides a text filter that moves dangling conjunctions from the end
of each line. It automatically adds a non-breaking space to the text where
necessary. This module skip ignore tags (a, script, style, code, pre) and does
not process attributes.

By default this filter is only for Polish language. It contains its own set of
words. Allow to add terms for any languages.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.

REQUIREMENTS
-------------

No specific requirements.

CONFIGURATION
-------------

1. Go to 'Text formats and editors' on the page '/admin/config/content/formats'
2. Add a new text format or edit an existing one
3. In the chosen text format, select the 'Hanging Conjunctions Filter' option
4. Change its order on the list, it should be after other filters that need to
be processed before, e.g. 'Correct faulty and chopped off HTML'
5. Click on the 'Save configuration' button

MAINTAINERS
-----------

Current maintainers:

 * Krzysztof Domański - https://www.drupal.org/user/3572982
