<?php

namespace Drupal\Tests\hanging_conjunctions_filter\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\hanging_conjunctions_filter\TextReplacement;

/**
 * @coversDefaultClass \Drupal\hanging_conjunctions_filter\TextReplacement
 * @group hanging_conjunctions_filter
 */
class TextReplacementTest extends UnitTestCase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->moduleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $container = new ContainerBuilder();
    $container->set('module_handler', $this->moduleHandler->reveal());
    \Drupal::setContainer($container);
  }

  /**
   * Tests adding non-breaking space after conjunction and specific words.
   *
   * @param string $text
   *   The source text.
   * @param string $expected
   *   The expected text.
   * @param string $langcode
   *   The langcode of the source text.
   *
   * @dataProvider providerHangingConjunctions
   */
  public function testProcessHangingConjunctions($text, $expected, $langcode = 'pl') {
    $replacement = new TextReplacement();
    $this->assertEquals($expected, $replacement->processHangingConjunctions($text, $langcode));
  }

  /**
   * Provides data for self::testProcessHangingConjunctions().
   *
   * @return array
   *   An array of test data.
   */
  public function providerHangingConjunctions() {
    return [
      [
        'Panu Zagłobie odwaga uderzyła niespodzianie do głowy jak wino. Być może, że to desperacja dała mu taki do czynu pochop; być może nadzieja, że pan Wołodyjowski jeszcze blisko, dość, że gołą szablą błysnął, oczyma zatoczył straszliwie i zakrzyknął:',
        'Panu Zagłobie odwaga uderzyła niespodzianie do&nbsp;głowy jak&nbsp;wino. Być może, że&nbsp;to&nbsp;desperacja dała mu taki do&nbsp;czynu pochop; być może nadzieja, że&nbsp;pan Wołodyjowski jeszcze blisko, dość, że&nbsp;gołą szablą błysnął, oczyma zatoczył straszliwie i&nbsp;zakrzyknął:',
      ],
      [
        'Augue dolor ea exerci i nimis pala refoveo o secundum tego.',
        'Augue dolor ea exerci i nimis pala refoveo o secundum tego.',
        'und',
      ],
      // Ignore case sensitive.
      [
        'ROK 1647 BYŁ TO DZIWNY ROK, W KTÓRYM ROZMAITE ZNAKI NA NIEBIE I ZIEMI ZWIASTOWAŁY JAKOWEŚ KLĘSKI I NADZWYCZAJNE ZDARZENIA.',
        'ROK 1647 BYŁ TO&nbsp;DZIWNY ROK, W&nbsp;KTÓRYM ROZMAITE ZNAKI NA&nbsp;NIEBIE I&nbsp;ZIEMI ZWIASTOWAŁY JAKOWEŚ KLĘSKI I&nbsp;NADZWYCZAJNE ZDARZENIA.',
      ],
      // Ensures that no attributes are processed.
      [
        '<div><a href="https://www.example.com">Example A i o</a> A i o <img src="/img/example.gif" alt="A i o" /><p> i </p></div>',
        '<div><a href="https://www.example.com">Example A i o</a> A&nbsp;i o&nbsp;<img src="/img/example.gif" alt="A i o" /><p> i&nbsp;</p></div>',
      ],
      [
        '<div class="content" title="a i o u w z">a i o u w z</div>a i o U w z',
        '<div class="content" title="a i o u w z">a i&nbsp;o u&nbsp;w z</div>a i&nbsp;o U&nbsp;w z',
      ],
      // Comments are not processed.
      [
        '<!--This is a comment.--><p>This is a comment.</p><!--This is a comment.-->',
        '<!--This is a comment.--><p>This is a&nbsp;comment.</p><!--This is a comment.-->',
      ],
      // Do not inject non-breaking space inside ignored tags.
      [
        '<style>/* a i o u w z */ body { background: #f3f3f3; } span, a {color: red } </style>',
        '<style>/* a i o u w z */ body { background: #f3f3f3; } span, a {color: red } </style>',
      ],
      [
        '<script>function example () { var i = 0; do { i += 1; } while ( i < 5 ); return i; }</script>',
        '<script>function example () { var i = 0; do { i += 1; } while ( i < 5 ); return i; }</script>',
      ],
      [
        '<code>function example () { var i = 0; do { i += 1; } while ( i < 5 ); return i; }</code>',
        '<code>function example () { var i = 0; do { i += 1; } while ( i < 5 ); return i; }</code>',
      ],
    ];
  }

}
