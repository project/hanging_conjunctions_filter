<?php

namespace Drupal\Tests\hanging_conjunctions_filter\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Hanging Conjunctions Filter.
 *
 * @group hanging_conjunctions_filter
 */
class HangingConjunctionsFilterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'hanging_conjunctions_filter',
    'language',
  ];

  /**
   * A random generated format machine name.
   *
   * @var string
   */
  protected $format;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Add additional languages.
    foreach (['cs', 'pl'] as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }

    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    // Create a text format with the Hanging Conjunctions Filter enabled.
    $this->format = strtolower($this->randomMachineName());
    FilterFormat::create([
      'format' => $this->format,
      'name' => $this->randomString(),
      'filters' => [
        'hanging_conjunctions_filter' => [
          'status' => 1,
        ],
      ],
    ])->save();

  }

  /**
   * Tests of adding additional terms in any languages.
   *
   * See hook_hanging_conjunction_filter_terms_alter().
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   *   Exception thrown for failed expectations.
   */
  public function testAddingAdditionalTerms() {
    $testedText = 'Augue dolor ea exerci i nimis pala xyz refoveo o secundum tego.';
    $nodes = [];

    // Add several articles in different languages.
    foreach (['und', 'en', 'cs', 'pl'] as $langcode) {
      $nodes[$langcode] = $this->drupalCreateNode([
        'type' => 'article',
        'title' => $this->randomString(),
        'langcode' => $langcode,
        'body' => [
          [
            'value' => $testedText,
            'format' => $this->format,
          ],
        ],
      ]);
    }

    $session = $this->assertSession();
    $this->drupalGet('node/' . $nodes['und']->id());
    $session->responseContains($testedText);
    $this->drupalGet('node/' . $nodes['en']->id());
    $session->responseContains($testedText);

    // Tests the value of the body field before altering terms.
    $this->drupalGet('node/' . $nodes['pl']->id());
    $session->responseContains('Augue dolor ea exerci i&nbsp;nimis pala xyz refoveo o&nbsp;secundum tego.');
    $this->drupalGet('node/' . $nodes['cs']->id());
    $session->responseContains($testedText);

    // Tests the value of the body field after altering terms.
    \Drupal::service('module_installer')->install(['terms_alter_test']);
    // Clear the cache to make changes visible.
    drupal_flush_all_caches();

    // An additional term 'xyz' has been added for the Polish language.
    $this->drupalGet('node/' . $nodes['pl']->id());
    $session->responseContains('Augue dolor ea exerci i&nbsp;nimis pala xyz&nbsp;refoveo o&nbsp;secundum tego.');
    // Single-letter words have been added for the Czech language.
    $this->drupalGet('node/' . $nodes['cs']->id());
    $session->responseContains('Augue dolor ea exerci i&nbsp;nimis pala xyz refoveo o&nbsp;secundum tego.');
  }

}
